﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class UIControllerScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	private bool cheatPlatformEnabled = false;
	public Text deathsText;
	private int deaths = 0;
	public int maxDeathsBeforeAd = 0;
	public GameObject cheatPlatform;
	public Text distanceText;

	void Awake()
	{
		deaths = GlobalValuesClass.deaths;
		cheatPlatformEnabled = GlobalValuesClass.cheatPlatformEnabled;
		deathsText.text = "Deaths : " + deaths.ToString();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		ShowCheatPlatform();
		distanceText.text = "Distance:" + Mathf.RoundToInt(GlobalValuesClass.distanceTraveled).ToString();

	}

	public void KillPlayer()
	{
		deaths++;
		deathsText.text = "Deaths : " + deaths.ToString();

		GlobalValuesClass.deaths = deaths;

		if(deaths % maxDeathsBeforeAd == 0) 
		{
			ShowAd();

		}

	}

	void ShowAd()
	{
		#if UNITY_EDITOR_WIN
		Debug.Log("Ad");
		#endif

		#if UNITY_ANDROID
		if(Advertisement.IsReady())
		{
			Advertisement.Show();
		}
		#endif
	}

	public void ResetButton()
	{
		SceneManager.LoadScene(0);

		Time.timeScale = 1;
	}

	public void OnPointerDown(PointerEventData data)
	{
		Debug.Log("Pointer down!");
	}

	public void OnPointerUp(PointerEventData data)
	{

	}

	public void ShowCheatPlatform()
	{
		cheatPlatform.GetComponent<MeshRenderer>().enabled = cheatPlatformEnabled;
		cheatPlatform.GetComponent<BoxCollider2D>().enabled = cheatPlatformEnabled;
	}

	public void CheatPlatformCheckChange()
	{
		cheatPlatformEnabled = !cheatPlatformEnabled;
	}

	void OnApplicationQuit()
	{

	}

}

