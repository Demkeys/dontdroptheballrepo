﻿/// <summary>
/// Level generator script - 11/03/16
/// 
/// Okay, starting from scratch. No idea what I'm doing. Just going based on
/// an idea that I have. So far the idea is...
/// At the start of the level, an obstaclePool will be generated procedurally.
/// Initially only sets of same obstacles will be shown, but once the end of the
/// obstaclesList is reached, the script will start generating pools of random
/// obstacles. I haven't worked on random obstacle pool generation yet though. 
/// Now, initially all the objects in the obstaclePool are disabled. The 
/// obstaclePool is an array of the ObstacleCollection class. The ObstacleCollection
/// class contains a List<GameObject> called obstaclesList. 
/// The plan is to activate and display gameobject from each cell of the obstaclePool
/// depending on the areaNumber. I'm yet to work on that part as well.
/// 
/// Update - 13/03/16
/// 
/// Alright so I need to comment this entire code before I forget what's going on and then
/// come back a month later and just stare at it wondering what I'm looking at. So here
/// goes...
/// This is the Level Generator Script. This script is meant to procedurally generate the
/// level. How it works is, at the start of the game, an Obstacles Set Pool is generated using
/// the items in the Obstacles List (which is populated before actually playing the game).
/// When the player passes the Procedural Gen Marker, the first set of obstacles from the 
/// obstaclesSetPool will be displayed. Right after the last obstacles in the set, the End 
/// Platform will be displayed. The Restart Gen Marker will be displayed based on the
/// position of the End Platform.
/// 
/// When the player passes the Restart Gen Marker, the player will get transported to the 
/// beginning of the level, and at this point areaNumber will increase
/// by one. Because the Start and End platforms are so long, hopefully
/// the player won't notice that he has been teleported and will just think the level is
/// going seemlessly. Now I'm not sure what happened here, but for some reason, when you pass
/// the Restart Gen Marker, not only is the player transported to the beginning, but the 
/// remainder of the level is also generated. I will look into that later but It's not 
/// really causing any problem as of now so it's okay. Anyways, if areaNumber is less than 
/// areaNumberToStartRandomization the next set of Obstacles will be displayed, otherwise,
/// Obstacles from the Random Obstacles Pool will be displayed. At that point, each time the 
/// player passes the Restart Gen Marker, the elements in the Random Obstacles Pool be 
/// reshuffled and then displayed. Again, based on the last element of the 
/// Random Obstacles Pool, the End Platform for Restart Gen Marker will be positioned.
/// 
/// Now I'm gonna go through the entire code to comment on the functions and logic.
/// 
/// </summary>
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class levelGeneratorScript : MonoBehaviour {

	public int areaNumberToStartRandomization = 0; // Decides when randomization starts
	public List<Obstacles> obstaclesList; // Populated from Inspector before running the game
	public ObstaclesCollection[] obstaclesSetPool; // Set length from Inspector
	public GameObject[] randomObstaclesPool; // Set length from Inspector
	public GameObject proceduralGenMarker; // Porcedural Gen starts when player passes this
	public GameObject restartGenMarker; // Player transported to start of level when he passes this
	public GameObject startPlatform; // Start Platform
	public GameObject endPlatform; // End Platform
	private int areaNumber = 0; // Area Number, helps with logic, and can also be used as Level No. for UI
	public GameObject playerObject; // Player
	private Vector3 initialPlayerPos; // Player's initial position
	private bool displayObstacles = false; // Whether or not to display obstacles from pool
	private bool shouldRestartGen = false; // Whether or not to transport player to beginning
	private bool passedProcGenMarker = false; // Has player passed ProcGenMarker?
	public float resetMarkerPositionOffset = 0f; // Offset from the last displayed obstacle
	public float endPlatformPositionOffset = 0f; // Offset from the last displayed obstacle
	public GameObject cheatPlatform; // Cheat platform to help with testing and debugging
	public float randomObstaclesSpace = 0f; // Space after random obstacle
	public Transform randomObstaclesStartLoc; // Where to start displaying random obstacles

	// Use this for initialization
	void Start () {

		initialPlayerPos = playerObject.transform.position;

		displayObstacles = true; // This has to be done to get the first set of
								// obstacles to show up.

		// Generate the entire Obstacle Pool at the start of the game.
		ProcedurallyGenerateObstaclesSet();
		ProcedurallyGenerateRandomObstacles();

	}

	// Update is called once per frame
	void Update () {
	
		// Check if player has passed the ProcGenMarker
		if(playerObject.transform.position.x > 
			proceduralGenMarker.transform.position.x)
		{
			passedProcGenMarker = true;

			// Check if player has passed the RestartGenMarker
			if(playerObject.transform.position.x >
				restartGenMarker.transform.position.x)
			{
				if(areaNumber < areaNumberToStartRandomization)
				{
					UndisplayObstaclesSetBasedOnAreaNumber(areaNumber);
				}
				else
				{
					UndisplayRandomObstacles();
				}
				displayObstacles = true;
				areaNumber++;
			}
		}
		else
		{
			passedProcGenMarker = false;
		}

		if(passedProcGenMarker && displayObstacles)
		{
			if(areaNumber < areaNumberToStartRandomization)
			{
				DisplayObstaclesSetBasedOnAreaNumber(areaNumber);
			}
			else
			{
				UpdateRandomObstaclesPool();
				DisplayRandomObstacles();
			}
			displayObstacles = false;
		}

	}

	void ProcedurallyGenerateObstaclesSet()
	{
		for(int i = 0; i < obstaclesList.Count; i++)
		{
			for(int j = 0; j < obstaclesList[i].numberOfCopies; j++)
			{
				GameObject obsGo = (GameObject)Instantiate(
					obstaclesList[i].gameObject);
				obsGo.SetActive(false);
				obsGo.transform.position = new Vector3(
					obstaclesList[i].position.x + obstaclesList[i].spaceAfterObstacle * j,
					obstaclesList[i].position.y,
					obstaclesList[i].position.z);
				
				obstaclesSetPool[i].obstaclesList.Add(obsGo);
			}
		}
	}

	void ProcedurallyGenerateRandomObstacles()
	{
		System.Random random = new System.Random();
		int index = 0;
		int prevIndex = 0;

		for(int i = 0; i < randomObstaclesPool.Length; i++)
		{
			index = random.Next(0, obstaclesList.Count-1);

			if(index == prevIndex) index = random.Next(0, obstaclesList.Count-1);

			GameObject go = (GameObject)Instantiate(
				obstaclesList[index].gameObject);
			go.transform.position = new Vector3(
				obstaclesList[index].position.x + obstaclesList[index].spaceAfterObstacle * i,
				obstaclesList[index].position.y,
				obstaclesList[index].position.z);
			go.SetActive(false);
			randomObstaclesPool[i] = go;
			prevIndex = index;
		}

	}

	// This function is going to reshuffle the elements
	// in randomObstaclesPool
	void UpdateRandomObstaclesPool()
	{
		System.Random random = new System.Random();
		for(int i = randomObstaclesPool.Length - 1; i > 0; i--)
		{
			int r = random.Next(0, i);
			GameObject g = randomObstaclesPool[i];
			randomObstaclesPool[i] = randomObstaclesPool[r];
			randomObstaclesPool[r] = g;
		}
	}

	void DisplayRandomObstacles()
	{
		for(int i = 0; i < randomObstaclesPool.Length; i++)
		{
			if(i == 0)
			{
				randomObstaclesPool[i].transform.position = new Vector3(
					randomObstaclesStartLoc.position.x + randomObstaclesSpace,
					randomObstaclesPool[i].transform.position.y,
					randomObstaclesPool[i].transform.position.z);
			}
			else
			{
				randomObstaclesPool[i].transform.position = new Vector3(
					randomObstaclesPool[i-1].transform.position.x + randomObstaclesSpace,
					randomObstaclesPool[i].transform.position.y,
					randomObstaclesPool[i].transform.position.z);
			}

			randomObstaclesPool[i].SetActive(true);
		}

		restartGenMarker.transform.position = new Vector3(
			randomObstaclesPool[randomObstaclesPool.Length-1].
			transform.position.x + resetMarkerPositionOffset,
			restartGenMarker.transform.position.y,
			restartGenMarker.transform.position.z);
		endPlatform.transform.position = new Vector3(
			randomObstaclesPool[randomObstaclesPool.Length-1].
			transform.position.x + endPlatformPositionOffset,
			startPlatform.transform.position.y,
			startPlatform.transform.position.z);

		ResetLevel();

	}

	void UndisplayRandomObstacles()
	{
		foreach(GameObject go in randomObstaclesPool)
		{
			go.SetActive(false);
		}

		playerObject.transform.position = new Vector3(
			initialPlayerPos.x,
			playerObject.transform.position.y,
			playerObject.transform.position.z);

	}

	void DisplayObstaclesSetBasedOnAreaNumber(int areaNo)
	{
		foreach(GameObject go in obstaclesSetPool[areaNo].obstaclesList)
		{
			go.SetActive(true);
		}

		restartGenMarker.transform.position = new Vector3(
			obstaclesSetPool[areaNo].obstaclesList[obstaclesSetPool[areaNo].obstaclesList.Count - 1].
			transform.position.x + resetMarkerPositionOffset,
			restartGenMarker.transform.position.y,
			restartGenMarker.transform.position.z);
		endPlatform.transform.position = new Vector3(
			obstaclesSetPool[areaNo].obstaclesList[obstaclesSetPool[areaNo].obstaclesList.Count - 1].
			transform.position.x + endPlatformPositionOffset,
			startPlatform.transform.position.y,
			startPlatform.transform.position.z);

		ResetLevel();

	}

	void UndisplayObstaclesSetBasedOnAreaNumber(int areaNo)
	{
		foreach(GameObject go in obstaclesSetPool[areaNo].obstaclesList)
		{
			go.SetActive(false);
		}

		playerObject.transform.position = new Vector3(
			initialPlayerPos.x,
			playerObject.transform.position.y,
			playerObject.transform.position.z);
	}

	void ResetLevel()
	{
		if(cheatPlatform.activeSelf)
		{
			Vector3 cheatPlatformPos = (
				startPlatform.transform.position + endPlatform.transform.position) * 0.5f;

			cheatPlatform.transform.position = new Vector3(
				cheatPlatformPos.x,
				cheatPlatform.transform.position.y,
				cheatPlatform.transform.position.z);

			float cheatPlatformScaleX = Vector3.Distance(
				new Vector3(
					startPlatform.GetComponent<BoxCollider2D>().bounds.max.x,
					cheatPlatform.transform.position.y,
					cheatPlatform.transform.position.z),
				new Vector3(
					endPlatform.GetComponent<BoxCollider2D>().bounds.min.x,
					cheatPlatform.transform.position.y,
					cheatPlatform.transform.position.z));

			cheatPlatform.transform.localScale = new Vector3(
				cheatPlatformScaleX,
				cheatPlatform.transform.localScale.y,
				cheatPlatform.transform.localScale.z);
		}
	}


}

[System.Serializable]
public class ObstaclesCollection
{
	public List<GameObject> obstaclesList;
}

[System.Serializable]
public class Obstacles
{
	public GameObject gameObject;
	public float spaceAfterObstacle = 0f;
	public int numberOfCopies = 0;
	public Vector3 position;
}