﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playerMoveScript : MonoBehaviour {

	public float moveForceX = 0f;
	public float accelMultiplier = 0f;
	[Tooltip("Supposed to be 10")]
	public float maxSpeed = 0f;

	public float jumpForce = 0f;
	public GameObject groundCheckObject;
	public float groundCheckDist = 0f;
	public LayerMask whatIsGround;
	private bool grounded = false;
	public bool canMultipleJump = false;
	public int maxJumps = 0;
	private int jumpCounter = 0;

	private Rigidbody2D rbody2D;
	public string KillObjectTag = "";
	public Vector3 accelerometer;
	private float h = 0f;
	public float final_h = 0f;
	public GameObject levelGen;
	public GameObject uiController;

	public float totalDistanceTraveled = 0f;
	private float currentXPosDiff = 0f; // Should be updated every frame.
	private float prevXPos = 0f; // Should be updated every frame.
	public Transform procGenMarker;
	public Transform restartMarker;
	private Vector3 initialPos;

	// Use this for initialization
	void Start () {
	
		rbody2D = GetComponent<Rigidbody2D>();
		initialPos = transform.position;
		prevXPos = initialPos.x;

	}

	void FixedUpdate()
	{
		final_h = (Mathf.Abs(rbody2D.velocity.x) < maxSpeed) ?
			h * accelMultiplier * moveForceX : 0f;


		rbody2D.AddForce(new Vector2(final_h, 0f));
	}

	// Update is called once per frame
	void Update () {

		CalculateDistanceTravelled();

		Screen.orientation = ScreenOrientation.Landscape;
	
		RaycastHit2D hit2d = Physics2D.Raycast(
			groundCheckObject.transform.position,
			-transform.up,
			groundCheckDist,
			whatIsGround);

		grounded = (hit2d.collider != null);


		if(Input.GetKeyDown(KeyCode.Space)) Jump();

		accelerometer = Input.acceleration;

	}

	void CalculateDistanceTravelled()
	{
		if(transform.position.x > prevXPos)
		{
			currentXPosDiff = transform.position.x - prevXPos;
			totalDistanceTraveled += currentXPosDiff;

			prevXPos = transform.position.x;
		}

		if(transform.position.x > restartMarker.position.x)
		{
			prevXPos = initialPos.x;	

		}

		GlobalValuesClass.distanceTraveled = totalDistanceTraveled;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.tag == KillObjectTag)
		{
			uiController.GetComponent<UIControllerScript>().KillPlayer();
			// Set the timeScale to zero, essentially pausing the game.
			Time.timeScale = 0;

			// Depending whether it is a sprite or mesh, the appropriate
			// component must be disabled.
			GetComponent<MeshRenderer>().enabled = false;

		}
	}

	public void GetHorizontalAxis(int axis)
	{
		h = axis;

	}

	public void JumpButtonClick(bool shouldJump)
	{
		if(shouldJump) Jump();

	}

	void Jump()
	{
		if(canMultipleJump) MultipleJump();
		else SingleJump();

	}

	void SingleJump()
	{
		if(grounded) rbody2D.AddForce(Vector2.up * jumpForce);
	}

	void MultipleJump()
	{
		if(jumpCounter < maxJumps)
		{
			rbody2D.AddForce(Vector2.up * jumpForce);
			jumpCounter++;
		}

		if(grounded)
		{
			jumpCounter = 1;
		}

	}

}
