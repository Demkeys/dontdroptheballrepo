﻿using UnityEngine;
using System.Collections;

public class MovingPlatformScript : MonoBehaviour {

	public float moveForceX = 0f;
	private Rigidbody2D rbody2D;
	public string bounceBlockTag = "";

	// Use this for initialization
	void Start () {
	
		rbody2D = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void Update () {
	
		rbody2D.velocity = Vector2.right * moveForceX;

	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag == bounceBlockTag)
		{
			moveForceX *= -1;
		}
	}

}
