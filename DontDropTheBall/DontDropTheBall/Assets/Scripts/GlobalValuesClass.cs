﻿using UnityEngine;
using System.Collections;

public static class GlobalValuesClass {

	public static int deaths = 0;
	public static bool cheatPlatformEnabled = false;
	public static float distanceTraveled = 0f;

}
