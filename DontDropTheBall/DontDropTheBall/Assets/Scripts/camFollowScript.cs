﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class camFollowScript : MonoBehaviour {

	public Transform player;
	public Vector3 posOffset;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		transform.position = new Vector3(
			player.position.x + posOffset.x,
			transform.position.y,
			transform.position.z);

	}
}
