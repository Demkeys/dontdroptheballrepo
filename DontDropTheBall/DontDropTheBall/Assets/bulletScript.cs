﻿using UnityEngine;
using System.Collections;

public class bulletScript : MonoBehaviour {

	public string playerTag = "";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag == playerTag)
		{
			Destroy(col.gameObject);
		}
		Destroy(gameObject);
	}

}
