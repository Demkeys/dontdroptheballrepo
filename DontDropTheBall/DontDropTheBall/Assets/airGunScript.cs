﻿using UnityEngine;
using System.Collections;

public class airGunScript : MonoBehaviour {

	public GameObject bullet;
	public float bulletForce = 0f;
	public float shootDelay = 0f;


	void OnEnable()
	{
		
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(shoot());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator shoot()
	{
		while(true)
		{
			yield return new WaitForSeconds(shootDelay);
			GameObject bul = (GameObject)Instantiate(
				bullet, transform.position, transform.rotation);
			bul.GetComponent<Rigidbody2D>().AddForce(transform.up * bulletForce);
		}
	}

}
